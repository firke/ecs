## ECS Technical Assessment

### Prerequisites

Requires mysql client to be installed locally, as it makes use of 'mysql' command.

Database deployed.

### Use

`./script.sh <directory with .sql scripts> <username for the DB> <DB host> <DB name> <DB password>`

### To-do
* Automation of the script
* Database bootstrapping
* Additional checks/error handling in the script
