#!/usr/bin/env bash
# Checks that 5 arguments are used
if [ $# != 5 ]; then
    printf "Your command line contains $# arguments, these are required: \n<directory with .sql scripts> <username for the DB> <DB host> <DB name> <DB password>."
    exit
fi
# Preliminary steps:
# export MySQL password to an env var, so mysql command does not give warnings
# regarding use of insecure passwords, some colours to be used later on;
export MYSQL_PWD=$5
red=$'\e[1;31m'
grn=$'\e[1;32m'
end=$'\e[0m'

printf "${red}Getting scripts from folder:${end}\n"
list_scripts=`find $1 -type f -name '[0-9]*.sql' -exec basename {} \; | sort -V`  # naturally sorted list of scripts in the folder
printf "$list_scripts\n" # prints the list

printf "${red}\nCurrent version of the DB:${end}\n"
current_version=`mysql --host $3 -u $2 $4 -Nse "SELECT version FROM versionTable LIMIT 1;"` # gets the current version from the DB
printf "${grn}$current_version${end}\n" # prints current version

printf "${red}\nChecking database for upgrades:\n${end}"

for script in $list_scripts # loops through the filenames in the list
do
  script_version=`echo $script | grep -Eo '[0-9]{1,3}'` # gets the version of the script in it's name
  if [ $current_version -lt $script_version ] # checks if DB version is lower than the scripts
  then
    printf "Running update ${grn}$script_version${end}.\n"
    mysql --host $3 -u $2 $4 -Ns < $1/$script # runs the SQL script
    mysql --host $3 -u $2 $4 -Nse "UPDATE versionTable SET version = $script_version" # updates the version in the DB
  elif [ $current_version -eq $script_version ] # checks if DB version matches script version
  then
    printf "No update is required, DB version is already ${grn}$script_version${end}."
  fi
done
